import Numberwang as nw
import Textwang as tw

from pylab import array,arange,plot,scatter,ylim,xlim,show

star = 0
step = 1
rang = 35000

thresh = 3

res = []
matches = []
soClose = []
txt=[]
for n in arange(star, star+rang+1, step):
    text = tw.wangString(n)
    tSum = nw.wangSum(text,tw.spacer)
    res.append([n,tSum])
    txt.append(text)
    if n==tSum:
        matches.append([n,tSum])
    elif abs(n-tSum) <= thresh:
        soClose.append([n,tSum])

res = array(res)
matches = array(matches)
soClose = array(soClose)
plot(res[:,0], res[:,1])
plot(soClose[:,0], soClose[:,1], 'x', markersize=12)
plot(matches[:,0], matches[:,1], 'o', markersize=10)
plot(res[:,0], res[:,0])
xlim(0,350)
ylim(0,350)
show()
