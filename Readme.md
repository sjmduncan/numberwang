Finds integers whose sum is equal to the sum of the letters in their
english-written form, if each letter is given the value of its position in the
alphabet.

Requires SciPy, python2
