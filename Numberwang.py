'''
@file Numberwang.py
@author Stu Duncan
Numberwang between the sum of the value of written numbers and the number which
they express
'''
from numpy import arange,array

letters = list("abcdefghijklmnopqrstuvwxyz")
numbers = range(1,27)
nulet = dict(zip(letters,numbers))

def stringToList(string, spacer=" ", stripSpace=True):
    chars = array(list(string))
    chars = chars[chars!=spacer]
    return chars

def wangSum(string, spacer=" "):
    chars = stringToList(string,spacer, True)
    wangSum = 0
    for c in chars:
        wangSum += nulet[c]
    return wangSum


def wangProd(string, spacer=" "):
    chars = stringToList(string, spacer, True)
    wangProd = 1
    for c in chars:
        wangProd = wangProd*nulet[c]
    return wangProd



