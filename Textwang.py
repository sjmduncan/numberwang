"""
@file Textwang.py
@author Stu Duncan
Convert integers to human-readable text numbers
"""
from math import log10 as log

spacer=" "
baseNums=["zero","one","two","three","four","five",\
            "six","seven","eight","nine",\
            "ten","eleven","twelve","thirteen","fourteen","fifteen",\
            "sixteen","seventeen","eighteen","nineteen","twenty"]
tensNums = ["ten","twenty","thirty","fourty","fifty",\
            "sixty","seventy","eighty","ninety"]
ordNums = ["ten", "hundred", "thousand", "million", "billion","trillion",\
        "quadrillion", "quintillion", "sextillion", "septillion", "octillion",\
        "nonillion", "decillion"]

stage = [[],baseNums,tensNums,ordNums]


def wangString(num):
    ss = ""
    while num>=1000:
        od = numOrder(num)
        osc = num/(10**(od[2]))
        ss = ss + wangScale(osc)
        ss = ss + spacer + ordNums[od[2]/3 + 1] + spacer
        num = num - osc * 10**od[2]
    if num < 100:
        ss = ss + "and" + spacer
    if num>0 and ss!="":
        ss = ss +wangScale(num)
    elif num > 0 or ss=="":
        ss = wangScale(num)
    else:
        ss = ss[:-1]
    return ss

    
def wangScale(num):
    if num < 21:
        return baseNums[num]
    elif num < 100:
        os = numOrder(num)
        pre = tensNums[os[1]-1]
        if (num-os[1]*10 > 0):
            pre = pre + spacer + baseNums[num-os[1]*10]
        return pre
    elif num < 1000:
        os = numOrder(num)
        pre = baseNums[os[1]] +spacer+ ordNums[1]
        n = num - os[1]*100
        if n>0:
            pre = pre + spacer + "and" + spacer +  wangScale(n)
        return pre
    else:
        return ''

def numOrder(n):
    if(n>0):
        o = int(log(n))
        ob = int(o/3)*3
    else:
        o=0
        ob=0
    return o, int(n/(10**o)), ob

def digitToWord(n):
    s = list(format(n))
    st = ""
    for c in s:
        i = int(c)
        st = st + baseNums[i]
    return st
